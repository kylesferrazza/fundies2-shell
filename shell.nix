{ pkgs ? import <nixpkgs> {},
  testerJar ? (builtins.fetchurl {
    url = "https://course.ccs.neu.edu/cs2510a/files/tester.jar";
    sha256 = "0icsn1klpp7s6fwfvmmbjd8xdnvfjkzjlf24rqidjw5k79s9x8dc";
  }),
  javalibJar ? (builtins.fetchurl {
    url = "https://course.ccs.neu.edu/cs2510a/files/javalib.jar";
    sha256 = "1v4rh3lixm3fvr5jc6w23637hrxz1pkqf365y0rgg7rk5bxds2q9";
  }),
}:
let
  runTester = pkgs.writeShellScriptBin "runTester" ''
    java -cp $CLASSPATH:$PWD tester.Main $@
  '';
in pkgs.mkShell {
  name = "fundies2";
  buildInputs = with pkgs; [
    racket
    jdk11
    eclipses.eclipse-java
    runTester
  ];
  CLASSPATH = pkgs.lib.concatStringsSep ":" [
    testerJar
    javalibJar
  ];
}
